const routes = require('express').Router();
const multer = require('multer');
const multerConfig = require('./config/multer');

const authMiddleware = require('./middlewares/Auth');

const PostController = require('./controllers/PostController');
const AuthController = require('./controllers/AuthController');
const CommentController = require('./controllers/CommentController');

//post routes
routes.get('/posts', PostController.index);
routes.get('/posts/:key',PostController.index);
routes.delete('/posts/:key',authMiddleware, PostController.remove);
routes.post('/posts',authMiddleware ,multer(multerConfig).single('file'),PostController.store);

//Auth routes
routes.post('/auth/register', AuthController.register);
routes.post('/auth/authenticate', AuthController.authenticate);


//commentRoutes
routes.post('/comment/:post_key',authMiddleware ,CommentController.store);
routes.get('/comment/:post_key',CommentController.index)
routes.delete('/comment/:id',authMiddleware ,CommentController.remove)

module.exports = routes;