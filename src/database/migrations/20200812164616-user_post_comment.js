'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('comment',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        user_id: {
          type: Sequelize.INTEGER,
          references: {model: 'user', key: 'id'},
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
        post_id: {
          type: Sequelize.INTEGER,
          references: {model: 'post', key: 'id'},
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        },
        comment:{
          type: Sequelize.TEXT,
          allowNull: false
        }

      });

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('comment');

  }
};
