const Sequelize = require('sequelize');
const dbConfig = require('../config/database');

const Post = require('../models/Post');
const User = require('../models/User');
const Comment = require('../models/Comment');

const connection = new Sequelize(dbConfig);

Post.init(connection);
User.init(connection);
Comment.init(connection);

User.sync();
Post.sync();
Comment.sync();

Post.associate(connection.models)
User.associate(connection.models)
Comment.associate(connection.models);

module.exports = connection;
