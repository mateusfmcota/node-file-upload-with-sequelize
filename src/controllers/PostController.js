const Post = require('../models/Post');
const User = require('../models/User');
const Comment = require('../models/Comment');

module.exports = {
    //comentario
    async index(req, res) {
        let posts;
        if (!req.params.key) {
            posts = await Post.findAll(
                {
                    attributes: { exclude: ['user_id'] },
                    include: {
                        association: 'user',
                        attributes: ['username']
                    }
                });
        } else {

            posts = await Post.findOne(
                {
                    where: { key: req.params.key },
                    attributes: { exclude: ['user_id'] },
                    include: {
                        association: 'user',
                        attributes: ['username']
                    }
                }
            );

            const comm = await Comment.findAll({
                where: { post_id: posts.id },
                attributes: ['comment'],
                include: {
                    association: 'user',
                    attributes: ['username']
                }
            });

            return res.json({posts, comments:comm});    
        }

        if (!posts)
            return res.status(400).send({ error: "Post does not exist" });

        return res.json(posts);
    },

    async store(req, res) {
        const { filename: name, size, key } = req.file
        if (name && size && key) {
            userId = req.userId;

            const user = await User.findByPk(userId);

            if (!user)
                return res.status(400).send({ error: "User not found" });


            const post = await Post.create({
                name,
                size,
                key,
                url: ''
            });

            user.addPost(post);

            return res.json(post);
        }
    },
    async remove(req, res) {
        const post = await Post.findOne({
            where: {
                key: req.params.key
            }
        });

        if (!post)
            return res.status(400).send({ error: "File does not exists" });

        await post.destroy();

        return res.sendStatus(200);
    }
};