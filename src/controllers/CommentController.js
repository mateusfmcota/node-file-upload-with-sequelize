const Post = require('../models/Post');
const User = require('../models/User');
const Comment = require('../models/Comment');

module.exports = {
    async index(req, res) {
        //needs a post and it will list all of it's comments
        const {post_key: key} = req.params;
        if (key) { //checks to see if came with a post id params

            const post = await Post.findOne({ where: { key } })
            // console.log(post);

            if (post) {
                //return comments from a post and username 
                const comm = await Comment.findAll({
                    where: { post_id: post.id },
                    attributes: ['comment'],
                    include: {
                        association: 'user',
                        attributes: ['username']
                    }
                });

                return res.json(comm);
            }

            //post does not exists
            return res.status(400).send({ error: "Post does not exists" });
        }

        //didn't put a post_id on params
        return res.status(400).send({ error: "Post_id not supplied" });
    },

    async store(req, res) {
        //create a comment
        
        const {post_key: key} = req.params;
        //needs user and post
        if (key) {
            const {comment} = req.body;

            const userId = req.userId;

            const post = await Post.findOne({ where: { key } })

            //check if post exists
            if (post) {
                //finding user
                const user = await User.findByPk(userId);

                if (!user)
                    return res.status(400).send({ error: "User not found" });

                //create comment
                comm = await Comment.create({
                    post_id: post.id,
                    user_id: userId,
                    comment
                });
                console.log(comm);
                console.log(comm.comment);
                return res.json(comm);
            }
        }

        //didn't put a post_id on params
        return res.status(400).send({ error: "Post_id not supplied" });
    },

    async remove(req, res) {
        const {id} = req.params;

        const comment = await Comment.findByPk(id);

        if (!comment)
            return res.status(400).send({ error: "Comment does not exists" });


        await comment.destroy();

        return res.sendStatus(200);

    }
}