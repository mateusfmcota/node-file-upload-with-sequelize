require('dotenv').config();
const path = require('path');

const dbConfig = {
    sqlite: {
        dialect: 'sqlite',
        storage: path.resolve(__dirname, '..', 'database', 'database.sqlite'),
        define: {
            timestamps: false
        }
    },
    mariadb :{
        host: process.env.MYSQL_HOST,
        username: process.env.MYSQL_USERNAME,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE,
        dialect: 'mariadb',
        dialectOptions: {connectTimeout: 1000},
        define:{
            timestamps: false
        }

    }, mysql:{
        host: process.env.MYSQL_HOST,
        username: process.env.MYSQL_USERNAME,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE,
        dialect: 'mysql',
        dialectOptions: {connectTimeout: 1000},
        define:{
            timestamps: false
        }

    },
    memory: {
        dialect: 'sqlite',
        storage: ':memory:',
        define: {
            timestamps: false
        }
    }
}
module.exports = dbConfig[process.env.DATABASE_TYPE];