const { Model, DataTypes } = require('sequelize');

//Comentario
class Comment extends Model {
    static init(sequelize) {
        super.init({
            comment: DataTypes.TEXT       
        }, {
            sequelize,
            tableName: 'comment'
        })

    }

    
    static associate(models){
        this.belongsTo(models.User, {foreignKey:'user_id', as: 'user'});
        this.belongsTo(models.Post, {foreignKey:'post_id', as: 'post'});
    }
}

module.exports = Comment;